package com.martrust.martrusttestservice.conversion.service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

import com.martrust.martrusttestservice.conversion.rest.api.CurrencyConversionResource;
import com.martrust.martrusttestservice.model.CurrencyConversion;
import com.martrust.martrusttestservice.model.LatestRates;
import com.martrust.martrusttestservice.provider.ExchangeRateClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
@RequiredArgsConstructor
public class ConversionService {

    private final ExchangeRateClient exchangeRateClient;

    @Value("${api.key}")
    private String apiKey;

    public Optional<CurrencyConversionResource> convert(String sourceCurrency, String targetCurrency, BigDecimal amountToConvert) {
        // cannot use convert endpoint due to API key limitations
        // CurrencyConversion currencyConversion = exchangeRateClient.convert(this.apiKey, sourceCurrency, targetCurrency, amountToConvert);

        BigDecimal conversionRate = this.retrieveCurrencyConversionRate(sourceCurrency, targetCurrency);
        BigDecimal convertedAmount = conversionRate.multiply(amountToConvert);
        return Optional.of(
            new CurrencyConversionResource()
                .setBaseRate(conversionRate)
                .setConvertedAmount(convertedAmount)
                .setSourceCurrency(sourceCurrency)
                .setTargetCurrency(targetCurrency)
        );
    }

    private BigDecimal retrieveCurrencyConversionRate(String sourceCurrency, String targetCurrency) {
        LatestRates latestRates = exchangeRateClient.getLatestRates(this.apiKey, null, null);
        if (null != latestRates && !CollectionUtils.isEmpty(latestRates.getRates())) {
            return latestRates.getRates()
                .entrySet()
                .stream()
                .filter(map -> map.getKey().equals(targetCurrency))
                .findFirst()
                .map(Map.Entry::getValue)
                .orElse(BigDecimal.ZERO);
        }
        return BigDecimal.ZERO;
    }

}
