package com.martrust.martrusttestservice.conversion.rest.controller;

import java.math.BigDecimal;

import com.martrust.martrusttestservice.conversion.rest.api.CurrencyConversionResource;
import com.martrust.martrusttestservice.conversion.service.ConversionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/convert")
@RequiredArgsConstructor
public class ConversionController {

    private final ConversionService conversionService;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CurrencyConversionResource> convertCurrency(@RequestParam String sourceCurrency,
                                                                      @RequestParam String targetCurrency,
                                                                      @RequestParam BigDecimal amountToConvert) {
        return ResponseEntity.ok(conversionService.convert(sourceCurrency, targetCurrency, amountToConvert).orElse(null));
    }

}
