package com.martrust.martrusttestservice.conversion.rest.api;

import java.math.BigDecimal;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CurrencyConversionResource {

    private String sourceCurrency;
    private String targetCurrency;
    private BigDecimal baseRate;
    private BigDecimal convertedAmount;

}
