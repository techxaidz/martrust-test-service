package com.martrust.martrusttestservice.provider;

import java.math.BigDecimal;

import com.martrust.martrusttestservice.model.CurrencyConversion;
import com.martrust.martrusttestservice.model.LatestRates;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "ExchangeRatesAPI", url = "http://api.exchangeratesapi.io/v1")
public interface ExchangeRateClient {

    @GetMapping("/latest")
    LatestRates getLatestRates(@RequestParam("access_key") String accessKey, @RequestParam("base") String baseCurrency,
                               @RequestParam("symbols") String currencyLimits);

    @GetMapping("/convert")
    CurrencyConversion convert(@RequestParam("access_key") String accessKey, @RequestParam("from") String sourceCurrency,
                               @RequestParam("to") String targetCurrency, @RequestParam("amount") BigDecimal amount);

}
