package com.martrust.martrusttestservice.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class CurrencyConversionInfo {

    private Long timestamp;
    private BigDecimal rate;

}
