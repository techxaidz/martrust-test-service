package com.martrust.martrusttestservice.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class CurrencyConversionQuery {

    private String from;
    private String to;
    private BigDecimal amount;

}
