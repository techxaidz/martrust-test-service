package com.martrust.martrusttestservice.model;

import java.math.BigDecimal;
import java.util.Map;

import lombok.Data;

@Data
public class LatestRates {

    private Boolean success;
    private Long timestamp;
    private String base;
    private Map<String, BigDecimal> rates;

}
