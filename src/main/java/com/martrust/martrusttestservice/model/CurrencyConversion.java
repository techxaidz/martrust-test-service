package com.martrust.martrusttestservice.model;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class CurrencyConversion {

    private boolean success;
    private CurrencyConversionQuery query;
    private CurrencyConversionInfo info;
    private boolean historical;
    private String date;
    private BigDecimal result;

}
