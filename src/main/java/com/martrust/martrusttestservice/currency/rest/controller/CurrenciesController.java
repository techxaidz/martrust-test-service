package com.martrust.martrusttestservice.currency.rest.controller;

import java.util.Collections;
import java.util.List;

import com.martrust.martrusttestservice.currency.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/currencies")
@RequiredArgsConstructor
public class CurrenciesController {

    private final CurrencyService currencyService;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> retrieveAllCurrencies() {
        return ResponseEntity.ok(currencyService.findAll().orElse(Collections.emptyList()));
    }

}
