package com.martrust.martrusttestservice.currency.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.martrust.martrusttestservice.model.LatestRates;
import com.martrust.martrusttestservice.provider.ExchangeRateClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CurrencyService {

    @Value("${api.key}")
    private String apiKey;

    private final ExchangeRateClient exchangeRateClient;

    public Optional<List<String>> findAll() {
        LatestRates latestRates = exchangeRateClient.getLatestRates(this.apiKey, null, null);
        return Optional.ofNullable(latestRates)
            .map(lr -> lr.getRates().keySet())
            .map(ArrayList::new);
    }

}
