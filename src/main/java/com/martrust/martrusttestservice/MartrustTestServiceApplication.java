package com.martrust.martrusttestservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MartrustTestServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MartrustTestServiceApplication.class, args);
	}

}
